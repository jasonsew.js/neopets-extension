function getElementByXpath(path) {
  return document.evaluate(
    path,
    document,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null
  ).singleNodeValue;
}

let table = getElementByXpath(
  "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/table/tbody"
);

let count = 0;
for (let row of table.rows) {
  for (let column of row.children) {
    if (column.innerHTML !== "&nbsp;") {
      count += 1;
    }
  }
}

getElementByXpath(
  "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/center[2]/form/input[2]"
).value = count;

getElementByXpath(
  "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/center[2]/form/input[3]"
).click();
