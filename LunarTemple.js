function getElementByXpath(path) {
  return document.evaluate(
    path,
    document,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null
  ).singleNodeValue;
}

let codes = getElementByXpath(
  "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/div[2]/div[1]/script[2]/text()"
);

let codeNumber = codes
  .splitText("")
  .wholeText.split("&")[1]
  .replace("angleKreludor=", "");

let number = Math.round(parseInt(codeNumber) / 22.5);

if (number === 16 || number < 8) {
  getElementByXpath(
    "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/div[2]/form/table/tbody/tr[2]"
  ).children[number % 8].children[2].click();
} else {
  getElementByXpath(
    "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/div[2]/form/table/tbody/tr[1]"
  ).children[number % 8].children[2].click();
}
