function getElementByXpath(path) {
  return document.evaluate(
    path,
    document,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null
  ).singleNodeValue;
}

let items = fetch(chrome.runtime.getURL("/items.json"))
  .then((resp) => resp.json())
  .then(function (items) {
    chrome.storage.sync.get(
      [
        "autoPurchase",
        "autoRefresh",
        "profitCalculator",
        "shops",
        "autoRedirect",
      ],
      async ({
        autoPurchase,
        autoRefresh,
        profitCalculator,
        shops,
        autoRedirect,
      }) => {
        let shopName =
          document.querySelector(".page-title__2020").children[1].innerText;

        let shopCheckInList = shops.some((shop) => {
          return shop.shopName === shopName;
        });

        let requestedShop;

        const shopList = shops.slice();
        if (!shopCheckInList) {
          requestedShop = {
            shopName: shopName,
            stayTime: 0,
            lastRecordTime: new Date(),
            bought: 0,
          };
          shopList.push(requestedShop);
        } else {
          requestedShop = shops.find((shop) => {
            return shop.shopName === shopName;
          });
        }

        if (
          (requestedShop.stayTime >= 3600000 + 1800000 ||
            (requestedShop.bought >= 2 && shopName)) &&
          autoRedirect
        ) {
          chrome.runtime.sendMessage(
            {
              cmd: "GET_URL",
              shopName: shopName,
            },
            (res) => {
              chrome.storage.sync.set({ shops: shopList });
              window.location.assign(res);
            }
          );
        }

        if (autoRefresh) {
          let sec = parseInt(Math.random() * 100000);
          while (sec < 16000 || sec > 32000) {
            sec = parseInt(Math.random() * 100000);
          }

          if (new Date() - requestedShop.lastRecordTime >= 15120000) {
            requestedShop.stayTime = 0;
            requestedShop.bought = 0;
          }

          let refreshCounterField = getElementByXpath("/html/body/div[13]/h2");
          let refreshCounter = Math.ceil(sec / 1000);
          setInterval(() => {
            refreshCounter -= 1;
            refreshCounterField.innerText =
              refreshCounter +
              "  " +
              Math.ceil(requestedShop.stayTime / 60000) +
              "mins";
          }, 1000);

          setTimeout(async () => {
            requestedShop.stayTime += sec;
            requestedShop.lastRecordTime = new Date().getTime();
            await chrome.storage.sync.get("shops", async ({ shops }) => {
              const newShopList = shops.slice();
              let shopToChange = newShopList.find((shop) => {
                return shop.shopName === shopName;
              });
              if (!shopToChange) {
                newShopList.push(requestedShop);
              } else {
                shopToChange.bought = requestedShop.bought;
                shopToChange.stayTime = requestedShop.stayTime;
                shopToChange.lastRecordTime = new Date(
                  requestedShop.lastRecordTime
                ).getTime();
              }
              await chrome.storage.sync.set({ shops: newShopList });
            });
            document.querySelector(".shop-bg").click();
          }, sec);
        }

        if (!document.querySelector(".shop-grid")) {
        } else {
          const inventory = Array.from(document.querySelectorAll(".shop-item"));

          for (let item of inventory) {
            let name = item.children[1].innerText.trim();
            let price = item.children[4].innerText.split(" ")[1];
            while (price.match(",")) {
              price = price.replace(",", "");
            }
            let response = await fetch(
              chrome.runtime.getURL("/itemToBought.json")
            );
            let itemsToBought = await response.json();
            for (let dbItem of items) {
              if (dbItem.itemName === name) {
                if (autoPurchase) {
                  if (
                    (dbItem.price - price >= 60000 && price <= 80000) ||
                    (dbItem.price - price >= 120000 && price <= 150000) ||
                    (dbItem.price - price >= 150000 && price <= 200000) ||
                    itemsToBought.find(
                      (itemToBought) => itemToBought.itemName === name
                    )
                  ) {
                    item.children[0].click();
                    document.querySelector("#confirm-link").click();
                    return;
                  }
                }

                if (profitCalculator) {
                  if (dbItem.price - price >= 10000) {
                    var alert = new Audio(
                      await chrome.runtime.getURL("/audio/remind.wav")
                    );
                    alert.play();
                    item.style.backgroundColor = "#4AEDFA";
                    item.scrollIntoView();
                  }

                  let profitReminder = document.createElement("p");
                  item.appendChild(profitReminder);
                  profitReminder.innerText = `profit : ${dbItem.price - price}`;
                  profitReminder.style.fontFamily =
                    "MuseoSansRounded500, Arial, sans-serif";
                  profitReminder.style.width = "90%";
                  profitReminder.style.margin = "5px auto";
                  profitReminder.style.textAlign = "center";
                  profitReminder.style.cursor = "default";
                  profitReminder.style.fontSize = "9pt";
                }
              }
            }
          }
        }
      }
    );
  });
