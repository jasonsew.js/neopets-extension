function getElementByXpath(path) {
  return document.evaluate(
    path,
    document,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null
  ).singleNodeValue;
}

const rejectMessage = document.querySelector(
  "#container__2020 > center:nth-child(9) > p:nth-child(2)"
);

const soldOutMessage = document.querySelector(
  "#container__2020 > p:nth-child(11) > b"
);

if (rejectMessage || soldOutMessage) {
  document.querySelector(".shop-bg").click();
}

chrome.storage.sync.get(
  ["autoFillPrice", "autoCaptcha"],
  ({ autoFillPrice, autoCaptcha }) => {
    if (autoFillPrice) {
      let priceHints = document.querySelector("#shopkeeper_makes_deal");
      if (!priceHints) return;

      let hintsArray = priceHints.innerText.replace(",", "").split(" ");

      for (let hint of hintsArray) {
        if (parseInt(hint)) {
          getElementByXpath("/html/body/div[11]/form/div/input").value =
            parseInt(hint);
          getElementByXpath(
            "/html/body/div[11]/form/div/div/input"
          ).scrollIntoView();
          break;
        }
      }
    }

    if (!autoCaptcha) return;
    var xhr = new XMLHttpRequest();

    const img = document.querySelector(
      "#container__2020 > form > div > div > input[type=image]"
    );

    xhr.open("GET", img.src, true);
    xhr.responseType = "blob";
    xhr.onload = function () {
      if (this.status === 200) {
        var blob = this.response;
        var reader = new FileReader();
        reader.onload = function (e) {
          var spark = new SparkMD5.ArrayBuffer();
          spark.append(e.target.result);
          const md5 = spark.end();

          fetch(chrome.runtime.getURL("/captcha.json"))
            .then((resp) => resp.json())
            .then(function ({ captchadata }) {
              const matchCaptcha = captchadata[md5];
              if (!matchCaptcha) {
                location.reload();
              } else {
                const captchaCoords = matchCaptcha.split(",");
                xLow = parseInt(captchaCoords[0]);
                yLow = parseInt(captchaCoords[1]);
                xHigh = parseInt(captchaCoords[2]);
                yHigh = parseInt(captchaCoords[3]);
                let { left, top } = img.getBoundingClientRect();
                let xClick = (xLow + xHigh) / 2;
                let yClick = (yLow + yHigh) / 2;

                Element.prototype._addEventListener =
                  Element.prototype.addEventListener;
                Element.prototype.addEventListener = function () {
                  let args = [...arguments];
                  let temp = args[1];
                  args[1] = function () {
                    let args2 = [...arguments];
                    args2[0] = Object.assign({}, args2[0]);
                    args2[0].isTrusted = true;
                    return temp(...args2);
                  };
                  return this._addEventListener(...args);
                };

                const ev = new MouseEvent("click", {
                  isTrust: true,
                  view: window,
                  bubbles: true,
                  cancelable: true,
                  clientX: xClick + left,
                  clientY: yClick + top,
                  x: xClick + left,
                  y: yClick + top,
                  pageX: xClick + left,
                  pageY: yClick + top,
                  offsetX: xClick,
                  offsetY: yClick,
                });
                const el = document.elementFromPoint(
                  xClick + left,
                  yClick + top
                );
                el.dispatchEvent(ev);
              }
            });
        };

        reader.readAsArrayBuffer(blob);
      }
    };
    xhr.send();
  }
);
