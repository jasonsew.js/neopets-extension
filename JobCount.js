function getElementByXpath(path) {
  return document.evaluate(
    path,
    document,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null
  ).singleNodeValue;
}

let successMessage = getElementByXpath(
  "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/div/b"
);

if (successMessage) {
  if (successMessage.innerText.match("You got the job!")) {
    chrome.storage.sync.get("jobsCount", ({ jobsCount }) => {
      let newDate = parseInt(
        new Date()
          .toLocaleString("en-US", { timeZone: "America/Los_Angeles" })
          .split("/")[1]
      );
      if (newDate - jobsCount.date == 1) {
        let newJobsCount = {};
        newJobsCount["date"] = newDate;
        newJobsCount.count = 0;
      } else {
        chrome.storage.sync.set({
          jobsCount: { count: jobsCount.count + 1, date: jobsCount.date },
        });
      }
    });
  }
}
