function getElementByXpath(path) {
  return document.evaluate(
    path,
    document,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null
  ).singleNodeValue;
}

let largestAmount = getElementByXpath(
  "/html/body/div[2]/div[3]/table/tbody/tr/td[2]/p[1]/b"
).innerText.replace(" NP", "");

while (largestAmount.match(",")) {
  largestAmount = largestAmount.replace(",", "");
}

let autoButton = document.createElement("button");
getElementByXpath(
  "/html/body/div[2]/div[3]/table/tbody/tr/td[2]/form/table/tbody/tr[2]/td"
).appendChild(autoButton);
autoButton.innerText = "Auto Withdraw";
autoButton.style.fontFamily = "Verdana, Arial, Helvetica, sans-serif";
autoButton.style.fontSize = "9pt";

autoButton.addEventListener("click", () => {
  getElementByXpath(
    "/html/body/div[2]/div[3]/table/tbody/tr/td[2]/form/table/tbody/tr[1]/td[2]/input"
  ).value = largestAmount;
  getElementByXpath(
    "/html/body/div[2]/div[3]/table/tbody/tr/td[2]/form/table/tbody/tr[2]/td/input"
  ).click();
});
