let items = fetch(chrome.runtime.getURL("/items.json"))
  .then((resp) => resp.json())
  .then((items) => {
    setTimeout(() => {
      let inventoryTable = Array.from(document.querySelectorAll(".item-name"));

      for (let inventory of inventoryTable) {
        let inventoryName = inventory.innerText;
        for (let item of items) {
          if (item.itemName === inventoryName) {
            let priceInfo = document.createElement("p");
            inventory.parentElement.appendChild(priceInfo);
            priceInfo.innerText = `price: ${item.price}`;
            priceInfo.style.fontFamily =
              "MuseoSansRounded500, Arial, sans-serif";
            priceInfo.style.width = "90%";
            priceInfo.style.margin = "5px auto";
            priceInfo.style.textAlign = "center";
            priceInfo.style.cursor = "default";
            priceInfo.style.fontSize = "9pt";
          }
        }
      }
    }, 1500);
  });
