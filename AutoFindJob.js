function getElementByXpath(path) {
  return document.evaluate(
    path,
    document,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null
  ).singleNodeValue;
}

chrome.storage.sync.get(
  ["autoDollJob", "isFirstRun"],
  ({ autoDollJob, isFirstRun }) => {
    if (!autoDollJob) return;

    if (
      getElementByXpath(
        "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/center[2]/table"
      )
    ) {
      let table = document.querySelector(
        "#content > table > tbody > tr > td.content > center:nth-child(10) > table > tbody"
      );

      let occur = false;

      for (let i = 1; i < table.children.length; i = i + 3) {
        let splitInnerText = table.children[i].innerText.split(" ");
        let lengthOfTr = splitInnerText.length;
        let profit = splitInnerText[lengthOfTr - 2].replace(",", "");
        if (parseInt(profit) >= 7000) {
          table.children[i].style.backgroundColor = "#147CC2";
          occur = true;
          let costReminder = document.createElement("p");
          table.children[i].appendChild(costReminder);
          costReminder.innerText = `Maximum Cost : ${
            parseInt(profit) / parseInt(splitInnerText[1])
          }`;
        }
      }

      if (!occur) {
        let prev = getElementByXpath(
          "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/center[2]/b[2]/a"
        );
        if (prev) {
          prev.click();
        } else {
          if (isFirstRun) {
            getElementByXpath(
              "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/center[2]/b[4]/a"
            ).click();
          }
          chrome.storage.sync.set({ isFirstRun: !isFirstRun });
        }
      }
    } else {
      getElementByXpath(
        "/html/body/div[3]/div[3]/table/tbody/tr/td[2]/center[2]/a/b"
      ).click();
    }
  }
);
