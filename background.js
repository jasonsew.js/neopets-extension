chrome.runtime.onInstalled.addListener(function () {
  chrome.storage.sync.get(
    [
      "autoPurchase",
      "autoFillPrice",
      "autoDollJob",
      "isFirstRun",
      "autoRefresh",
      "profitCalculator",
      "shops",
      "jobsCount",
      "autoCaptcha",
      "currentShops",
      "purchaseHistory",
    ],
    (res) => {
      if (!res.hasOwnProperty("autoPurchase")) {
        chrome.storage.sync.set({
          autoPurchase: false,
          autoFillPrice: false,
          autoDollJob: false,
          isFirstRun: true,
          autoRefresh: false,
          profitCalculator: false,
          autoCaptcha: false,
          autoRedirect: false,
          shops: [],
          currentShops: [],
          purchaseHistory: [],
          jobsCount: {
            count: 0,
            date: parseInt(
              new Date()
                .toLocaleString("en-US", { timeZone: "America/Los_Angeles" })
                .split("/")[1]
            ),
          },
        });
      }
    }
  );
});

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  switch (message.cmd) {
    case "GET_URL":
      chrome.storage.sync.get(["shops"], ({ shops }) => {
        const shopList = shops.slice();
        for (let shop of shopList) {
          if (new Date() - shop.lastRecordTime >= 15120000) {
            shop.stayTime = 0;
            shop.bought = 0;
          }
        }
        chrome.storage.sync.set({ shops: shopList });

        fetch(chrome.runtime.getURL("/shops.json"))
          .then((resp) => resp.json())
          .then(async (shopsData) => {
            let queryOptions = {
              url: "*://www.neopets.com/objects.phtml?*",
            };
            let tabs = await chrome.tabs.query(queryOptions);
            const tabShops = [];

            for (let tab of tabs) {
              tabShops.push({
                tabId: tab.id,
                shopName: tab.title.split(" - ")[1],
              });
            }

            let shopsAvoid = shopList.filter((shop) => {
              let isShopOpening = tabShops.some((currentOpeningShop) => {
                return shop.shopName === currentOpeningShop.tabShopName;
              });
              return (
                shop.stayTime >= 3600000 + 1800000 ||
                shop.bought >= 1 ||
                isShopOpening
              );
            });

            tabShops.forEach((tabShop) => {
              let isAvoided = false;
              for (let shopAvoid of shopsAvoid) {
                if (tabShop.shopName === shopAvoid.shopName) {
                  isAvoided = true;
                }
              }
              if (!isAvoided) {
                shopsAvoid.push(tabShop);
              }
            });

            const chooseShop = (shopData) => {
              if (shopsAvoid.length === 0) {
                return shopData;
              }

              let toBeAvoid = false;
              for (let shopAvoid of shopsAvoid) {
                if (shopAvoid.shopName === shopData.shopName) {
                  toBeAvoid = true;
                }
              }

              if (!toBeAvoid) return shopData;
            };

            let priority1 = shopsData.priority1.find(chooseShop);
            let priority2 = shopsData.priority2.find(chooseShop);
            let priority3 = shopsData.priority3.find(chooseShop);

            if (priority1) {
              sendResponse(priority1.url);
            } else if (priority2) {
              sendResponse(priority2.url);
            } else if (priority3) {
              sendResponse(priority3.url);
            }
          });
      });
    default:
      break;
  }
  return true;
});
