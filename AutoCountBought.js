chrome.storage.sync.get(
  ["shops", "purchaseHistory"],
  ({ shops, purchaseHistory }) => {
    const shopName = document.querySelector(
      "#container__2020 > div.page-title__2020 > h1"
    ).innerText;

    let shopCheckInList = shops.some((shop) => {
      return shop.shopName === shopName;
    });

    let requestedShop;
    const newShopList = shops.slice();

    if (!shopCheckInList) {
      requestedShop = {
        shopName: shopName,
        stayTime: 0,
        lastRecordTime: new Date(),
        bought: 0,
      };
    } else {
      requestedShop = newShopList.find((shop) => {
        return shop.shopName === shopName;
      });
    }

    const addToInventoryMsg = document.querySelector(
      "#container__2020 > p:nth-child(10)"
    );

    if (addToInventoryMsg) {
      const newPurchaseHistory = purchaseHistory.slice();
      const priceMessage = document
        .querySelector("#container__2020 > p:nth-child(9)")
        .innerText.split(" ");
      let price;
      for (let priceMessageString of priceMessage) {
        if (parseInt(priceMessageString)) {
          price = parseInt(priceMessageString);
        }
      }

      let item = addToInventoryMsg.innerText.split(" ")[0];

      requestedShop.bought += 1;

      if (!shopCheckInList) {
        newShopList.push(requestedShop);
      }

      if (item && price) {
        newPurchaseHistory.push({
          price: price,
          itemName: item,
          acc: document.querySelector(
            "#navprofiledropdown__2020 > div:nth-child(3) > a"
          ).innerText,
        });
      }

      chrome.storage.sync.set({ shops: newShopList });
      chrome.storage.sync.set({ purchaseHistory: newPurchaseHistory });
    }

    document.querySelector("#container__2020 > div.shop-bg").click();
  }
);
