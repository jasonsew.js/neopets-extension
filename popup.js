const autoPurchaseInput = document.querySelector("#auto-purchase");
const autoFillPriceInput = document.querySelector("#auto-fill-price");
const autoDollJobInput = document.querySelector("#auto-doll-job");
const autoRefreshInput = document.querySelector("#auto-refresh");
const profitCalculatorInput = document.querySelector("#profit-calculator");
const autoCaptchaInput = document.querySelector("#auto-captcha");
const autoRedirectInput = document.querySelector("#auto-redirect");
const jobCountOutField = document.querySelector(".job-count");
const stayTimeChecker = document.querySelector("#stay-time-checker");
const purchaseHistoryChecker = document.querySelector(
  "#purchase-history-checker"
);
const clearStateButton = document.querySelector("#clear-state");
const clearStayTimeButton = document.querySelector("#clear-stay-time");
const inputs = document.querySelectorAll(".checkBox");

chrome.storage.sync.get(
  [
    "autoPurchase",
    "autoFillPrice",
    "autoDollJob",
    "autoRefresh",
    "profitCalculator",
    "jobsCount",
    "autoCaptcha",
    "autoRedirect",
  ],
  ({
    autoPurchase,
    autoFillPrice,
    autoDollJob,
    autoRefresh,
    profitCalculator,
    jobsCount,
    autoCaptcha,
    autoRedirect,
  }) => {
    autoPurchaseInput.checked = autoPurchase;
    autoFillPriceInput.checked = autoFillPrice;
    autoDollJobInput.checked = autoDollJob;
    autoRefreshInput.checked = autoRefresh;
    autoCaptchaInput.checked = autoCaptcha;
    profitCalculatorInput.checked = profitCalculator;
    jobCountOutField.innerText = jobsCount.count;
    autoRedirectInput.checked = autoRedirect;
  }
);

window.onload = () => {
  chrome.storage.sync.get(
    ["shops", "purchaseHistory"],
    ({ shops, purchaseHistory }) => {
      if (shops.length >= 0) {
        let newShopList = shops.slice();
        for (let shop of newShopList) {
          if (typeof shop.shopName !== "string") continue;

          if (new Date() - shop.lastRecordTime >= 15120000) {
            shop.stayTime = 0;
          }
          let newOption = document.createElement("option");
          stayTimeChecker.appendChild(newOption);
          newOption.innerText = shop.shopName;
          newOption.value = shop.shopName;
        }

        let stayTimeOfShop = document.createElement("span");
        document.querySelector("#stay-time-field").appendChild(stayTimeOfShop);
        stayTimeChecker.addEventListener("change", (e) => {
          if (e.target.value === "") {
            stayTimeOfShop.innerText = "";
          }

          let selectedShop = newShopList.find((shop) => {
            return shop.shopName === e.target.value;
          });
          stayTimeOfShop.innerText = `${selectedShop.shopName} : ${Math.ceil(
            selectedShop.stayTime / 60000
          )}`;
        });
        chrome.storage.sync.set({ shops: newShopList });
      }

      if (purchaseHistory.length !== 0) {
        purchaseHistory.forEach((singlePurchase, i) => {
          if (
            typeof singlePurchase.itemName !== "string" ||
            typeof singlePurchase.price !== "number"
          )
            return;

          let newOption = document.createElement("option");
          purchaseHistoryChecker.appendChild(newOption);
          newOption.innerText = singlePurchase.itemName;
          newOption.value = i;
        });

        let purchaseHistorySpan = document.createElement("span");
        document
          .querySelector("#purchase-history-field")
          .appendChild(purchaseHistorySpan);
        purchaseHistoryChecker.addEventListener("change", (e) => {
          if (e.target.value === "") {
            purchaseHistorySpan.innerText = "";
          }

          let selectedPurchase = purchaseHistory[e.target.value];
          purchaseHistorySpan.innerText = `${selectedPurchase.itemName} : ${selectedPurchase.price}NP`;
        });
      }
    }
  );
};

for (let input of inputs) {
  input.addEventListener("click", (e) => {
    let object = {};
    let stateName = e.target.getAttribute("data-name");
    object[stateName] = e.target.checked;
    chrome.storage.sync.set(object);
  });
}

clearStayTimeButton.addEventListener("click", () => {
  chrome.storage.sync.set({ shops: [] });
});

clearStateButton.addEventListener("click", async () => {
  await chrome.storage.sync.clear();
  await chrome.storage.sync.set({
    autoPurchase: false,
    autoFillPrice: false,
    autoDollJob: false,
    isFirstRun: true,
    autoRefresh: false,
    profitCalculator: false,
    autoCaptcha: false,
    autoRedirect: false,
    shops: [],
    currentShops: [],
    purchaseHistory: [],
    jobsCount: {
      count: 0,
      date: parseInt(
        new Date()
          .toLocaleString("en-US", { timeZone: "America/Los_Angeles" })
          .split("/")[1]
      ),
    },
  });
});
