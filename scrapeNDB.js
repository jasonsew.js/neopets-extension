const puppeteer = require("puppeteer");
var fs = require("fs");

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.setUserAgent(
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
  );

  const scrape = async (page, start) => {
    await page.goto(
      `https://items.jellyneo.net/search/?min_rarity=1&max_rarity=300&sort=5&sort_dir=desc&limit=75&start=${start}/`,
      { waitUntil: "load", timeout: 0 }
    );

    const items = await page.evaluate(() => {
      let arr = document.querySelectorAll("ul")[14].childNodes;
      const returnArr = [];
      for (let i of arr) {
        if (i.innerHTML === undefined) {
          continue;
        } else {
          if (i.childNodes[6] === undefined) {
            continue;
          }
          let price = i.childNodes[6].innerText;
          while (price.includes(",")) {
            price = price.replace(",", "");
          }

          price = parseInt(price);

          if (!price) {
            returnArr.push({
              itemName: i.childNodes[3].innerText,
              price: 0,
            });
            continue;
          }

          returnArr.push({
            itemName: i.childNodes[3].innerText,
            price: price,
          });
        }
      }
      return returnArr;
    });

    return items;
  };

  let allItems = [];

  for (let i = 0; i <= 62062; i += 75) {
    allItems = allItems.concat(await scrape(page, i));
    console.log(i + "/62062");
  }

  const itemInJson = JSON.stringify(allItems);

  fs.writeFileSync("items.json", itemInJson);

  await browser.close();
})();
